<?php
/**
 * @file
 * Administrative forms section
 */

/**
 * Administration form for at_color settings.
 */
function at_color_admin_form($form, &$form_state) {
    $form['#attached']['css'] = array(
        'misc/farbtastic/farbtastic.css',
    );

    $form['#attached']['js'] = array(
        'misc/farbtastic/farbtastic.js',
        drupal_get_path('module', 'at_color') . '/js/at_color.js',
    );

    $form['color_picker'] = array(
        '#prefix' => '<p>Enter a valid css background value [i.e. #00ff00, or rgb(0, 255, 0)] 
        for the admin toolbar. Use the Color Picker below to set Hex values. Also, A handy tool can
        be found here <a href="https://www.w3schools.com/colors/colors_picker.asp">
        https://www.w3schools.com/colors/colors_picker.asp</a>. Darker colors are recommended due to the white text in the administration toolbar</p>',
        '#suffix' => '<div id="colorpicker"></div>',
    );
    $form['default_color'] = array(
        '#type' => 'item',
        '#required' => TRUE,
        '#title' => null,
        '#default_value' => variable_get('at_color'),
        '#attribute' => array(
            'class' => array('colorpicker-input'),
        ),

    );
  $form['at_color'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('CSS Background Color Value for Admin Toolbar'),
    '#default_value' => variable_get('at_color'),

  );


  return system_settings_form($form);
}
